<?php

App::uses('Controller', 'Controller');


class AppController extends Controller {

	public $helpers = array('Session','Html', 'Form', 'Paginator','Js');

	public $components = array('Auth' => array('authorize' => 'Controller'),
			'Session',
			'RequestHandler',
			'Acl',
			'Groups');

	public $uses = array ('Permission');

	public function isAuthorized($user = null) {
		if ($this->Groups->isAuthorized($user))
			return true;
		else {
			$this->Session->setFlash('Você não possui as permissões necessárias para acessar esse conteúdo');
			return false;
		}
	}

	public function beforeFilter() {

		//Configure AuthComponent
		$this->Auth->loginAction = array('controller' => 'users', 'action' => 'login');
		$this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login');
		$this->Auth->loginRedirect = array('controller' => 'users', 'action' => 'login');
		$this->Auth->deny('*');

	}

	public function email (){
		App::uses('CakeEmail', 'Network/Email');
		$email = new CakeEmail();
		$email = new CakeEmail('contact');
		$email->from(array('guilherme.torres@sga.pucminas.br' => 'My Site'));
		$email->to('guilherme.torres@sga.pucminas.br');
		$email->subject('About');
		$email->send('My message');;
		die;
	}
}
?>
