<?php
class GroupsComponent extends Component {
	
	public $components = array('Acl','Session','Auth' => array('authorize' => 'Controller'),'RequestHandler');
	
	public $uses = array ('Permission');
	
	public $request = array ();
	
	public function initialize($controller){
		$this->request = $controller->request;
		$aro_id = $this->Auth->user('aro_id');
		//if user is logged and has an aro_id
		$allowedActions = $this->Session->read('Auth.User.allowedActions');
		if ($aro_id && !$allowedActions){
			$allowedActions = $this->getAllowedActions($aro_id);
			if ($this->Session->write('Auth.User.allowedActions',$allowedActions));
		}
		$controller->set('allowedActions',$allowedActions);
	}
	
	private function getAllowedActions ($aro_id){
		//find all actions allowed for the aro
		$permission = ClassRegistry::init('Permission');		
		$acos = $permission->find('all',array (
				'recursive' => 0,
				'conditions' => array (
						'aro_id' => $this->Auth->user('aro_id'),
						'Permission._create' => '1',
						'Permission._update' => '1',
						'Permission._read' => '1',
						'Permission._update' => '1',
				),
				'fields' => array ('Aco.alias','Aco.id','Aco.parent_id'))
		);
		//make the array more readable
		$actions = Set::classicExtract($acos,'{n}.Aco');
		//get all parents_id (controllers)
		if (!$actions)
			return false;		
		$parents_id = array_unique(Set::classicExtract($actions,'{n}.parent_id'));
		//query to find all parents id alias (contollers names)
		$query = array ('OR' => array ());
		foreach ($parents_id as $parent){
			array_push ($query['OR'], array ('Aco.id' => $parent));
		}
		//find all controllers
		$controllers = $this->Acl->Aco->find('list',array(
				'fields' => 'alias',
				'conditions' => $query)
		);
		//create an array in this format
		// ['controllerName'] = array (actions);
		$actions = Set::combine($actions,'{n}.id','{n}.alias','{n}.parent_id');
		foreach ($actions as $parent_id => $action){
			$action = array_flip ($action);
			$action = array_change_key_case($action,CASE_UPPER);
			//debug ($action);
			$actions[$controllers[$parent_id]] = $action;
			//debug ($action);
			unset ($actions[$parent_id]);
		}
		$actions = array_change_key_case($actions,CASE_UPPER);
		return $actions;
	}

	public function isAuthorized($user = null) {
		if (!$user['active']){
			$this->Session->setFlash('Seu usuário não está ativo, em caso de dúvidas ...');
			return false;
		}
		if (!isset($user['allowedActions']))
			return false;
		$controller = $this->request->params['controller'];
		$action = $this->request->params['action'];
		
		$path = 'allowedActions.' . strtoupper("$controller.$action"); 
		return Set::check($user,$path);	
	}
	
	
}