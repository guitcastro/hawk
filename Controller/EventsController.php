<?php
App::uses('AppController', 'Controller');
/**
 * Events Controller
 *
 * @property Event $Event
 */
class EventsController extends AppController {
	
	public $layout = 'default';
	
	public function beforeFilter (){
		parent::beforeFilter();
		$this->Auth->allow(array ('view','filter','index'));
	}

	public function filter($eventType) {				
		if ($eventType != 'curso' && $eventType != 'workshop' && $eventType != 'palestra')
			throw new NotFoundException(__('Categoria inválida'));
		$this->Event->recursive = -1;
		$events = $this->paginate(array (
				'Event.type' => array ('Event.type' => $eventType))
		);
		$this->set('events' , $events);
		$this->render('eventcategory');
	}

/**
 * index method
 *
 * @return void
 */
        public function index() {
                $this->Event->recursive = 0;
                $this->set('events', $this->paginate());
        }

/**
 * view method
 *
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Event->id = $id;
		if (!$this->Event->exists()) {
			throw new NotFoundException(__('Evento inválido'));
		}
		$this->set('event', $this->Event->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Event->create();
			if ($this->Event->save($this->request->data)) {
				$this->Session->setFlash(__('O evento foi salvo.'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Não foi possível salvar o evento.'));
			}
		}
	}

/**
 * edit method
 *
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Event->id = $id;
		if (!$this->Event->exists()) {
			throw new NotFoundException(__('Invalid event'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Event->save($this->request->data)) {
				$this->Session->setFlash(__('The event has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The event could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->Event->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Event->id = $id;
		if (!$this->Event->exists()) {
			throw new NotFoundException(__('Invalid event'));
		}
		if ($this->Event->delete(null,true)) {
			$this->Session->setFlash(__('Evento deletado'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Evento não foi deletado'));
		$this->redirect(array('action' => 'index'));
	}
}
