<?php

App::uses('AppController', 'Controller');

/**
 * Groups Controller
 *
 * Manage aros and acos objects,
 * Acos are only controllers and actions;
 * Aros are only users and Groups
 * @author Guilherme Torres Castro
 */
class GroupsController extends AppController {
	
	public $layout = 'admin';

	//properties

	public $uses = array('Aro');
	public $paginate = array('Aro'=> array(
		'update' => '#content',
		'evalScripts' => true,
		'conditions' => array ('Aro.model' => 'Group'),		
        'limit' => 5)
	); 

	/**
	 * index method	
	 */
	public function index() {
		$aros = $this->paginate();
		$groups = $this->_getAroControllers($aros);
		$groups = $this->_getAroActions($groups);
		$this->set('groups',$groups);
	}

	/**
	 * Create a new aro
	 */
	public function add() {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->request->data['Aro']['parent_id'] = null;
		$this->request->data['Aro']['model'] = 'Group';
		$this->Acl->Aro->create();
		if ($this->Acl->Aro->save($this->request->data))
		$this->Session->setFlash(__('The group has been saved'));
		else
		$this->Session->setFlash(__('The group could not be saved. Please, try again.'));
		$this->_createRelationship($this->request->data['Aro']['alias']);
		$this->redirect(array('action' => 'index'));
	}

	/**
	 * delete the spcefic aro
	 *
	 * @param int $id $aro id 
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Aro->id = $id;
		if (!$this->Aro->exists()) {
			throw new NotFoundException(__('Invalid User Group'));
		}
		if ($this->Acl->Aro->delete()) {
			$this->Session->setFlash(__('User Group deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('User Group was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	/**
	 * Edit the specific aro permission
	 */
	public function edit() {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->render('index');
		$aro = $this->request->data['Group'];
		$aroAlias = key($aro);
		$controllers = current ($aro);
		foreach ($controllers as $controller => $actions){
			$controllerAlias = $controller;
			foreach ($actions as $action => $permission){
				if ($permission){
					$this->Acl->allow($aroAlias ,"$controllerAlias/$action");
				}
				else{					
					$this->Acl->deny($aroAlias ,"$controllerAlias/$action");
				}
			}
		}
		$this->redirect(array('action' => 'index'));
	}

	//privates methods

	/**
	 * Create relationship between an aro and all acos in the database
	 * @param string $aroAlias the aro alias, left in blank to create relationship for all aros
	 */
	private function _createRelationship($aro) {
		$acos = $this->Acl->Aco->find('threaded',array (
		'fields' => array ('alias','id','parent_id'),
		'recursive' => -1 ));
		if (!$aro){
			foreach ($this->Acl->Aro->find('list',array ('fields'=> 'alias')) as $aro)
			$this->recursivePermission($aro,$acos);
		}else
		$this->_recursivePermission($aro,$acos);
	}

	/**
	 * Assign permission recursive
	 * @param string $aro the aro alias
	 * @param array  $aco an array in tree format: [0] => [Aco]
	 * 													  [children]
	 * 													   ...
	 * @param string $path do not suply this param, its here just for the recursion
	 */
	private function _recursivePermission ($aro,$acos,$path = ''){
		foreach ($acos as $aco){
			if (!$this->Acl->deny($aro,$path . $aco['Aco']['alias']))
			$this->Acl->deny($aro,$path . $aco['Aco']['alias']);
			$this->_recursivePermission ($aro,$aco['children'],$path . $aco['Aco']['alias'] .'/');
		}
	}

	/**
	 * Read an aco_aro object and transform in array
	 * in this format :
	 * [aro] => [id] => aro_id
	 * 			[alias] => aro_alias
	 * 			[controllers] => array (
	 * 									[id] => aco_controller_id
	 * 									[alias] => aco_controller_id
	 * 									[actions] => array ()
	 * 									)
	 * 							  ...
	 *
	 * 			[acos] => array ()
	 * 			others acos (that no reference controllers acos, in most
	 * 			cases, actions.Are hold in there, for fill the [actions]
	 * 			array for each controller (see above).
	 * 			this data is temporary (just for performace) and will be delete
	 * 			when this acos fill the '$controller['action']' array.
	 *
	 *
	 *	@param array $aros
	 *  @return array
	 */
	private function _getAroControllers ($aros){
		$groups = array ();
		$acos = array ();
		//assign controllers for the groups
		foreach ($aros as $aro){
			//get the 'controller' root node id
			$rootNode = current($aro['Aco']);
			$acoControllerId = $rootNode['id'];
			$group = array ('id'=> $aro['Aro']['id'],
							'alias' => $aro['Aro']['alias'],
							'controllers' => array(),
							'acos' => array ()
			);
			foreach ($aro['Aco'] as $aco){
				//if aco is a Controller ...
				if ($aco['parent_id'] === $acoControllerId)
				$group['controllers'][$aco['id']] =  array ('id'=>$aco['id'],
															'alias' => $aco['alias'],
															'actions' => array()
				);
				else
				//if is an action, hold it to add as child of controller
				//this data will be delete
				array_push($group['acos'],$aco);
			}
			array_push($groups,$group) ;
		}
		return $groups;
	}

	/**
	 *
	 * Modified the $group array (created at _getAroControllers)
	 * to fill the actions array withe their respective actions
	 * @param array $group an array created using _getAroControllers
	 * @return an well formated array of aro permisson
	 */
	private function _getAroActions ($groups){
		//assign actions and their permissions for the groups
		foreach ($groups as $key => $value){
			$acos = $value['acos'];
			unset($groups[$key]['acos']);
			foreach ($acos as $aco){
				$parent_id = $aco['parent_id'];
				if (isset($groups[$key]['controllers'][$parent_id ])){
					array_push ($groups[$key]['controllers'][$parent_id]['actions'],
					array ('alias' => $aco['alias'],
							'permission' =>	$this->_checkPermission($aco['Permission'])));
				}
			}
		}
		return $groups;
	}


	/**
	 * Check if all permitions (CRUD) are granted
	 * @param array $permissions An aco_aro permission
	 * @return boolean
	 */
	private function _checkPermission ($permissions){
		return (
		$permissions['_create'] == 1 &&
		$permissions['_read'] == 1 &&
		$permissions['_update'] == 1 &&
		$permissions['_delete'] == 1 );
	}
}
