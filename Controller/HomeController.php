<?php

App::uses('AppController', 'Controller');
class HomeController extends AppController {

	public $uses = array('DaysEvent', 'Image');
	
	public function beforeFilter (){
		parent::beforeFilter();
		$this->Auth->allow('*');
		$this->set("title_for_layout","Hawk Investimentos");
	}

	/**
	 * Carregas as imanges a serem usadas no Carousel
	 * cache: Um dia
	 */
	public function index() {
		$images = Cache::read('images_carousel', 'images');
		if (!$images){
			$images = $this->Image->find('all', array(
					'order' => 'Image.from DESC',
					'recursive' => 0,
					'fields' => array('webPath', 'title', 'link'),
					'conditions' => array(
							'Image.from <=' => date("Y-m-d"),
							'Image.until >=' => date("Y-m-d"))
			));
			Cache::write('images_carousel',$images, 'images');
		}
		$this->set('images',$images);
	}

	/**
	 * Retorna um json com todos os eventos realizados no mês
	 * especificado
	 * @param int $month mês que deseja listar o evento, ou o mês atual se não especificado
	 */
	public function findEvents($month = null) {
		if (!$month)
			$month = date('m');
		$nextMonth = $month + 1;
		$events = $this->DaysEvent->find('all', array(
				'order' => 'DaysEvent.day',
				'recursive' => 1,
				'conditions' => array(
						'DaysEvent.day >' => date("Y-$month-00"),
						'DaysEvent.day <' => date("Y-$nextMonth-00"))
		));
		$calendar = array();
		foreach ($events as $event) {
			$arrayDate = getDate(strtotime($event['DaysEvent']['day']));
			$day = $arrayDate['mday'];
			//se já não existir nenhuma evento no dia, criar a chave
			if (!isset($calendar[$day]))
				$calendar[$day] = array($event['Event'] + array ('event_id' => $event['DaysEvent']['event_id']));
			//se já exite a chave, adiconar outro evento
			else				
				array_push($calendar[$day],array($event['Event'] + array ('event_id' => $event['DaysEvent']['event_id'])));
		}
		$this->autoRender = false;
		$this->response->header('Content-type: application/json');
		echo json_encode($calendar);
	}

	/**
	 * Renderiza o feed especificado
	 * cache de 10 minutos
	 * @param string $feed o nome do feed a ser renderizado (estadao , exame ou infomoney)
	 * @throws InternalErrorException Quando não é possicel ler o feed
	 */
	public function feed ($feed){
		if ($feed === 'estadao')
			$feedUrl = 'http://economia.estadao.com.br/EN/rss/ultimas.xml';
		else if ($feed === 'exame')
			$feedUrl = 'http://feeds.feedburner.com/Exame-Negocios?format=xml';
		else if ($feed === 'infomoney')
			$feedUrl = 'http://www.infomoney.com.br/acoes/rss';
		$this->layout = 'ajax';
		$xml = Cache::read($feed, 'rss');
		if (!$xml){
		        try {
	          	   $cURL = curl_init($feedUrl);
    		           curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
		           $xml = Xml::toArray(Xml::build(curl_exec($cURL)));
		           curl_close($cURL);
			   Cache::write( $feed ,$xml ,'rss');	
			}catch (Exception $e){
			   throw new InternalErrorException ('Não foi possivel se conectar com o feed');
			}
		}
		$news = $xml['rss']['channel']['item'];
		$this->set('news',$news);
	}
}
