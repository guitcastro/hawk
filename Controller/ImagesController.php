<?php
App::uses('AppController', 'Controller');
/**
 * Images Controller
 *
 * @property Image $Image
 */


App::import('Utility', 'File');

class ImagesController extends AppController {
	
	public $layout = 'admin';


	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->Image->recursive = 0;
		$this->set('images', $this->paginate());
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {

		if ($this->request->is('post')) {
			$path = IMAGES;
			$requestData = $this->request->data;
			$fileName = str_replace(array('/',' '), '_', $requestData['Image']['file']['name']);
			$fileUploaded = new File($requestData['Image']['file']['tmp_name']);
			$fileUploadedData = $fileUploaded->read();
			$Newfile = new File($path .$fileName,true);
			$Newfile->write($fileUploadedData);
			$requestData['Image']['webPath'] =  IMAGES_URL . $fileName;
			$requestData['Image']['serverPath'] = IMAGES . $fileName;
			$this->Image->create();
			if ($this->Image->save($requestData)) {
				$this->Session->setFlash(__('A imagem salva com sucesso'));
                                Cache::delete('images_carousel', 'images');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Ocorreu um erro ao tentar salvar a imagem, por favor verifique novamente o formulário.'));
			}
		}
	}

	/**
	 * delete method
	 *
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Image->id = $id;
		if (!$this->Image->exists()) {
			throw new NotFoundException(__('Imagem inválida'));
		}
		if ($this->Image->delete()) {
			$this->Session->setFlash(__('Imagem exluida com sucesso'));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Não foi possível deleter a imagem'));
		$this->redirect(array('action' => 'index'));
	}
}
