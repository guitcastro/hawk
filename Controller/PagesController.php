<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       Cake.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

	/**
	 * Default helper
	 *
	 * @var array
	 */

	public $uses = array('InternalPage');
	
	public function beforeFilter (){
		parent::beforeFilter();
		$this->Auth->allow('display','edit');
	}

	/**
	 * Displays a view
	 *
	 * @param mixed What page to display
	 * @return void
	 */
	public function display($internalPage) {
		$page = $this->InternalPage->find('first' , array ('conditions' => array ('InternalPage.slug' => $internalPage)));
		if (!$page)
			throw new NotFoundException(__('Página não encontra'));
		$this->set('page',$page);
		$this->set('title_for_layout',$page['InternalPage']['name']);
		$this->render ('internal');
	}
	
	public function edit($id) {
		$this->InternalPage->id = $id;
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->InternalPage->save($this->request->data)) {
				$this->Session->setFlash(__('Página editada com sucesso'));
				$this->redirect(array('controller' => 'pages' , 'action' => 'display',$this->request->data['InternalPage']['slug'] ));
			} else {
				$this->Session->setFlash(__('Não foi possível editar a página.'));
			}
		} else {
			$this->request->data = $this->InternalPage->read(null, $id);
		}
	}
}
