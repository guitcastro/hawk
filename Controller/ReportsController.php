<?php
App::uses('AppController', 'Controller');

App::import('Utility', 'File');

class ReportsController extends AppController {

	
	public function beforeFilter (){
		parent::beforeFilter();
		$this->Auth->allow('index');
	}

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->Layout = 'admin';
		$this->Report->recursive = 1;
		$this->set('reports', $this->paginate());
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {			
			$requestData = $this->request->data;
			$path = WWW_ROOT . 'files' . DS;
			$fileName = str_replace(array('/',' '), '_', $requestData['Report']['file']['name']);
			$requestData['Report']['webPath'] =  'files/' . $fileName;
			$requestData['Report']['serverPath'] = $path  . $fileName;
				
			$fileUploaded = new File($requestData['Report']['file']['tmp_name']);
			$fileUploaded->copy($path .$fileName,true);
		
			$this->Report->create();
			if ($this->Report->save($requestData)) {
				$this->Session->setFlash(__('The report has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The report could not be saved. Please, try again.'));
			}
		}
	}

	/**
	 * delete method
	 *
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Report->id = $id;
		if (!$this->Report->exists()) {
			throw new NotFoundException(__('Relatório inválido'));
		}
		if ($this->Report->delete()) {
			$this->Session->setFlash(__('Report deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Report was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
