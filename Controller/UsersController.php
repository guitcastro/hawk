<?php

App::uses('AppController', 'Controller');

class UsersController extends AppController {

	public $helpers = array('Paginator', 'Html', 'Form','UI','Time');
	public $uses = array('User', 'Login');	

	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow('login','logout','showLogins');
		$this->layout = 'admin';
	}

	private function removeUnsetData (){
		unset ($this->request->data['Email']['X']);
		unset ($this->request->data['Phone']['X']);
		if (!$this->request->data['Phone'])
			unset ($this->request->data['Phone']);
		if (!$this->request->data['Email'])
			unset ($this->request->data['Email']);
	}

	public function login() {
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				$this->Login->create();
				$login = array ('Login' => array ('user_id' => $this->Auth->user('id'), 'login_date' => date ("Y-m-d")));
				$this->Login->save($login);						
				$this->redirect((array ('controller' => 'home')));
			} else {
				$this->Session->setFlash('Senha ou login incorretos.');
			}
		}
	}

	/**
	 * logout user and redirect to refer
	 */
	public function logout() {
		$this->Auth->logout();
		$this->redirect($this->referer());
	}

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add() {
		$aros = $this->Acl->Aro->find('list',array ('fields' => 'alias'));
		$this->set('aros',$aros);
		if ($this->request->is('post')) {
			$this->removeUnsetData();
			$this->request->data['User']['password'] = 'hawkinvestimentos';
			$this->User->create();
			if ($this->User->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
	}

	/**
	 * edit method
	 *
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null) {
		$this->User->id = $id;
		$aros = $this->Acl->Aro->find('list',array ('fields' => 'alias'));
		$this->set('aros',$aros);
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Usuário inválido'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			$this->removeUnsetData();
			if ($this->User->saveAssociated($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Não foi possível editar o usuário.'));
			}
		} else {
			$this->request->data = $this->User->read(null, $id);
		}
	}

	/**
	 * delete method
	 *
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->User->delete()) {
			$this->Session->setFlash(__('User deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('User was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	public function showEmails ($userId){
		$emails = $this->User->Email->find ('list',array (
				'fields' => 'email',
				'conditions' =>  array ('Email.user_id' => $userId))
		);
		$this->set('emails',$emails);
	}
	
	public function showLogins ($userId){
		$logins = $this->User->Login->find ('all',array (
				'conditions' =>  array ('Login.user_id' => $userId),
				'fields' => 'DISTINCT login_date',
				)
		);
		$this->set('logins',set::classicExtract($logins,'{n}.Login.login_date'));
	}

	public function showPhones ($userId){
		$phones = $this->User->Phone->find ('list',array (
				'fields' => 'number',
				'conditions' =>  array ('user_id' => $userId))
		);
		$this->set('phones',$phones);
	}

	public function deleteEmail ($id){
		$this->autoRender = false;
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->User->Email->id = $id;
		if (!$this->User->Email->exists()) {
			throw new NotFoundException(__('Invalid email'));
		}
		return $this->User->Email->delete($id);
	}

	public function deletePhone ($id){
		$this->autoRender = false;
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->User->Phone->id = $id;
		if (!$this->User->Phone->exists()) {
			throw new NotFoundException(__('Invalid email'));
		}
		return $this->User->Phone->delete($id);
	}

	public function changePassword ($id){
		if ($this->request->is('post')){
			$user  =  $this->User->read ('password',$id);
			if (!$this->User->exists()) {
				throw new NotFoundException(__('Usuário inválido'));
			}
			else if (!$this->request->data['User']['password'] === $this->request->data['User']['passwordConfirm']){
				$this->Session->setFlash ('A confirmação de senha não confere com a nova senha');
			}
			else if (AuthComponent::password($this->request->data['User']['oldPassword']) === $user['User']['password']){
				if ($this->User->saveField('password',$this->request->data['User']['password']))
					$this->Session->setFlash ('Senha alterada com sucesso.');
				else 
					$this->Session->setFlash('Não foi possível alterar a senha.');
			}else {
				$this->Session->setFlash ('A senha atual é inválida.');
			}
			$this->redirect($this->referer());
		}
	}

}