<?php
App::uses('AppModel', 'Model');

class Email extends AppModel {

	public $validate = array(
			'email' => array(
					'isEmail' => array (
							'rule'    => 'email',
							'allowEmpty' => false,
							'message' => 'Endereço de email inválido.'),
					'isUnique' => array(
							'rule'    => 'isUnique',
							'allowEmpty' => false,
							'message' => 'O endereço de email já foi cadastrado.')
			)				
	);

	public $belongsTo = array(
			'User' => array(
					'className' => 'User',
					'foreignKey' => 'user_id')
	);

}
