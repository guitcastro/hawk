<?php
App::uses('AppModel', 'Model');
/**
 * Event Model
 *
 * @property Created $Created
 * @property Modified $Modified
 * @property Day $Day
 */
class Event extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'title';
	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
			'title' => array(
					'notempty' => array(
							'rule' => array('notempty'),
							'message' => 'O titúlo do evento deve ser informado',
					),
			),
	);


	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
			'Creator' => array(
					'className' => 'User',
					'foreignKey' => 'created_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			),
			'Modifier' => array(
					'className' => 'User',
					'foreignKey' => 'modified_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			)
	);

	function afterSave (){
		App::import('Model','DaysEvent');
		$days = new DaysEvent;
		if (isset($this->data['day'])){
			foreach ($this->data['day'] as $day){
				$days->create();
				$dayEvent = array ('DaysEvent' => array ('event_id' =>$this->data['Event']['id'], 'day' =>$day . ' 00:00:00' ));
				$days->save($dayEvent);
			}
		}
	}

	/**
	 * hasAndBelongsToMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
			'Days' => array(
					'className' => 'DaysEvent',
					'dependent' => true
			)
	);

}
