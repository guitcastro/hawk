<?php
App::uses('AppModel', 'Model');
/**
 * Image Model
 *
 */
class Image extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'O título deve ser preenchido',
			),
		),		
		'link' => array(
			'numeric' => array(
				'rule' => array('url'),
				'message' => 'O link deve ser uma url válida',
				'allowEmpty' => true,
				'required' => false,
			),
		),
	);
}
