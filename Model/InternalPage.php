<?php
App::uses('AppModel', 'Model');
/**
 * InternalPage Model
 *
 */
class InternalPage extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';
}
