<?php
App::uses('AppModel', 'Model');

class Phone extends AppModel {
/*	
	public $validate = array(
			'number' => array(
					'notempty' => array(
							'rule' => array('notempty'),
							'message' => 'O telefone deve ser preenchido',
							'allowEmpty' => false,
							'required' => false,
							'last' => false, // Stop validation after this rule
					)
			)
	);
*/
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id'
		)
	);
}
