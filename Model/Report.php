<?php
App::uses('AppModel', 'Model');
/**
 * Report Model
 *
 */
class Report extends AppModel {
/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';
	
	public $actsAs = array('Tags.Taggable');
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
			),
		),
	);
}
