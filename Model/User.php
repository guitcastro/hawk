<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 * @property Aro $Aro
 * @property Email $Email
 * @property Login $Login
 * @property Phone $Phone
 */
class User extends AppModel {
	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'name';
	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
			'name' => array(
					'notempty' => array(
							'rule' => array('notempty'),
							'message' => 'O nome do usuário deve ser preenchido',
							'allowEmpty' => false,
							'required' => true,
							'last' => false, // Stop validation after this rule
					),
			),
			'username'=> array(
					'notempty' => array(
							'rule' => array('notempty'),
							'message' => 'O login deve ser preenchido',
							'allowEmpty' => false,
							'required' => true,
							'last' => false, // Stop validation after this rule
					),
					'numeric' => array(
							'rule' => array('numeric'),
							'message' => 'O login deve ser numérico',
							'allowEmpty' => false,
							'required' => true,
							'last' => false,
					)
			)
	);

	/**
	 * belongsTo associations
	 *
	 * @var array
	 */
	public $belongsTo = array(
			'Aro' => array(
					'className' => 'Aro',
					'foreignKey' => 'aro_id',
					'conditions' => '',
					'fields' => '',
					'order' => ''
			)
	);

	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
			'Email' => array(
					'className' => 'Email',
					'foreignKey' => 'user_id',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			),
			'Login' => array(
					'className' => 'Login',
					'foreignKey' => 'user_id',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			),
			'Phone' => array(
					'className' => 'Phone',
					'foreignKey' => 'user_id',
					'dependent' => false,
					'conditions' => '',
					'fields' => '',
					'order' => '',
					'limit' => '',
					'offset' => '',
					'exclusive' => '',
					'finderQuery' => '',
					'counterQuery' => ''
			)
	);

	function parentNode() {
		if (!$this->id && empty($this->data)) {
			return null;
		}
		$data = $this->data;
		if (empty($this->data)) {
			$data = $this->read();
		}
		if (!$data['User']['aro_id']) {
			$data['User']['aro_id'] = 1;
		} else {
			$this->Aro->id = $data['User']['aro_id'];
			$groupNode = $this->Aro->node();
			return array('Aro' => array('id' => $groupNode[0]['Aro']['foreign_key']));
		}
	}

	public function bindNode($user) {
		return array('model' => 'Group', 'foreign_key' => $user['User']['group_id']);
	}

	public function beforeSave ($options = array()){
		if (isset ($this->data['User']['password']))
			$this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
		return parent::beforeSave();
	}
	
}
