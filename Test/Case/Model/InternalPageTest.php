<?php
/* InternalPage Test cases generated on: 2012-03-07 21:22:57 : 1331166177*/
App::uses('InternalPage', 'Model');

/**
 * InternalPage Test Case
 *
 */
class InternalPageTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.internal_page');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->InternalPage = ClassRegistry::init('InternalPage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->InternalPage);

		parent::tearDown();
	}

}
