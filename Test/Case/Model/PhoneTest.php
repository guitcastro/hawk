<?php
/* Phone Test cases generated on: 2012-02-14 15:16:21 : 1329239781*/
App::uses('Phone', 'Model');

/**
 * Phone Test Case
 *
 */
class PhoneTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.phone', 'app.user');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Phone = ClassRegistry::init('Phone');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Phone);

		parent::tearDown();
	}

}
