<div id="login">
	<div id="login-btn">
		<span>Área Restrita</span> <span id="arrow"></span>
	</div>
	<div class="dashboard">
		<ul>
			<?php if (isset ($allowedActions['REPORTS'])) {
				echo '<li>';
				echo $this->Html->link ('Relatórios',array ('controller' => 'reports'));
				echo '</li>';
			}?>
			<?php if (isset ($allowedActions['EVENTS'])) {
				echo '<li>';
				echo $this->Html->link ('Eventos',array ('controller' => 'events'));
				echo '</li>';
			}?>
			<?php if (isset ($allowedActions['GROUPS'])) {
				echo '<li>';
				echo $this->Html->link ('Grupos',array ('controller' => 'groups'));
				echo '</li>';
			}?>
			<li><a href="/images" > Imagens </a></li>
			<?php if (isset ($allowedActions['USERS'])) {
				echo '<li>';
				echo $this->Html->link ('Usuários',array ('controller' => 'users'));
				echo '</li>';
			}?>
			<?php
			echo '<li>';
			echo $this->Js->link ('Trocar minha senha',array (
					'controller' => 'users' ,
					'action' => 'changePassword' ,
					$this->Session->read('Auth.User.id')),
					array ('update' => '#placeHolder')
			);
			echo '</li>'; ?>
			<?php
			echo '<li>';
			echo $this->Html->link ('Sair',array ('controller' => 'users' , 'action' => 'logout'));
			echo '</li>'; ?>
		</ul>
	</div>
</div>
