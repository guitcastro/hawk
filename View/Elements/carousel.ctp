<div id="carousel" class="jcarousel-skin-tango">
	<ul>
		<?php		 
		foreach ($images as $image){
			$imgTag = "<img src=\"{$this->webroot}{$image['Image']['webPath']}\" alt=\"{$image['Image']['title']}\"  />";
			if ($image['Image']['link'] && !$image['Image']['link'] == "")
				echo "<li><a href=\"{$image['Image']['link']}\" >$imgTag</a></li>";
			else
				echo "<li>$imgTag</li>";
		}?>
	</ul>
	<div class="jcarousel-control">
		<?php for ($i=1;$i<=count($images);$i++){
			echo "<a href=\"$i\">$i</a>";
		};
		?>
	</div>
</div>
<script type="text/javascript">
$("#carousel").jcarousel({
		scroll: 1,
		visible : 1,
		wrap: "both",
		auto: 6,
		initCallback: function (carousel){
			jQuery(".jcarousel-control a").bind("click", function() {
			carousel.scroll(jQuery.jcarousel.intval(jQuery(this).attr("href")));
			return false;
			});
		},
		itemVisibleInCallback : function (carousel,li,position){
			a = $(".jcarousel-control").find(":contains("+position+")");			
			a.addClass('current');
		},
		itemVisibleOutCallback : function (carousel,li,position){
			a = $(".jcarousel-control").find(":contains("+position+")");			
			a.removeClass('current');
		}
	});
</script>