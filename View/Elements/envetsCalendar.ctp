<div id="datepickerEvents"></div>

<script type="text/javascript">

//carregar eventos paga exbir nos calendários
events = null;
function loadEvents (month){
	var url = '/home/findEvents/' + month;		
	$.ajax({
		url: url ,  
		dataType: 'json',  		
		async: false,  
		success: function(json){ 
			events = json; 
		}  
	});
}

	/* DATEPICKER */
	$(document).ready(function () {	
	loadEvents ((new Date()).getMonth() + 1);
	$('#datepickerEvents').datepicker({
		dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
		dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
		dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
		monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro', 'Outubro','Novembro','Dezembro'],
		monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set', 'Out','Nov','Dez'],
		nextText: 'Próximo',
		prevText: 'Anterior',
		onSelect: function(dateText, inst) { 
			var day = parseInt(dateText.split ('/')[1]);			
			var event = events[day]['0'];
			$(location).attr('href','/events/view/' +  event['event_id']);			
		},
		onChangeMonthYear: function(year, month, inst) { 
			loadEvents (month);
		},
	    beforeShowDay: function(date) {
			if (events != null && events[date.getDate()]){
				var daysEvents = events[date.getDate()];
				var styleClass = '';
				var eventsNames = '';
				var eventsTypes = {workshop : false, palestra : false, curso :false }; 
				for (var i  in daysEvents){
					eventsTypes[daysEvents[i].type] = true;					
					//&#xD; &#xA; &#13; &#10; works on chrome
					eventsNames += daysEvents[i].title + ' &#10;';
				}				
				if (eventsTypes.workshop)
					styleClass += 'workshop';
				if (eventsTypes.curso)
					styleClass += ' curso';
				if (eventsTypes.palestra)
					styleClass += ' palestra';
				return [true,styleClass,eventsNames];
			}
			else
				if (new Date().getDate() == date.getDate()){
					return [true];
				}
				else
					return false;
			}		
	});
	});
	</script>