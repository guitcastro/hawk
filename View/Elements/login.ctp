<div id="login">
	<div id="login-btn">
		<span>Login</span> <span id="arrow"></span>
	</div>
	<!--/login-btn-->

	<div id="login-form" class="dashboard">
		<?php echo $this->Form->create('User',array ('action' => 'login'));?>
		<fieldset>
			<?php echo $this->Form->input('username', array(
					'type' => 'text',
					'div' => false,
					'placeholder' => 'nome de usuário',
					'class' => 'username field',
					'label' => false)); ?>
			<?php 
			echo $this->Form->input('password', array(
					'type' => 'password',
					'placeholder' => 'senha',
					'div' => false,
					'class' => 'field',
					'label' => false));

			echo $this->Form->submit('Enviar', array( 'type' => 'submit',
					'class' => 'submit',
					'div' =>false,
					'label' => false
			));?>
			<span class="clear"></span>
			<div class="connected">
				<?php echo $this->Form->input ('permanecer conectado', array('class' => '', 'type' => 'checkbox')); ?>
			</div>
			<!--/connected-->
			<span class="clear"></span> <a href="#" class="forgot-password">esqueci
				minha senha</a>
		</fieldset>
		<?php echo $this->Form->end();?>
	</div>
	<!--/login-form-->
</div>
<!--/login-->
