<div id="quotes">
	<div id="ibove" class="item">
		<span class="arrow"></span> <span class="indice-name ibove"> IBOV </span>
		<div class="price">
			Price: <span >N/A </span>
		</div>
		<div class="change">
			Change: <span class="value">N/A </span>
		</div>
		<span class="clear"></span>
	</div>
	<div id="nasdaq" class="item">
		<span class="arrow"></span> <span class="indice-name nasdaq"> IBOV </span>
		<div class="price">
			Price: <span >N/A </span>
		</div>
		<div class="change">
			Change: <span class="value">N/A </span>
		</div>
		<span class="clear"></span>
	</div>
	<div id="sp500" class="item">
		<span class="arrow"></span> <span class="indice-name sp500"> IBOV </span>
		<div class="price">
			Price: <span >N/A </span>
		</div>
		<div class="change">
			Change: <span class="value">N/A </span>
		</div>
		<span class="clear"></span>
	</div>
	<div id="dowJones" class="item last">
		<span class="arrow"></span> <span class="indice-name dowjones"> IBOV </span>
		<div class="price">
			Price: <span >N/A </span>
		</div>
		<div class="change">
			Change: <span class="value">N/A </span>
		</div>
		<span class="clear"></span>
	</div>
</div>
<script type="text/javascript">
function getQuotes(){    
	   $.ajax({
	      url : "http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20csv%20where%20url%3D%27http%3A%2F%2Fdownload.finance.yahoo.com%2Fd%2Fquotes.csv%3Fs%3D%255EBVSP%2C%255EIXIC%2C%255EGSPC%26f%3Dsl1d1t1c1ohgv%26e%3D.csv%27%20and%20columns%3D%27symbol%2Cprice%2Cdate%2Ctime%2Cchange%2Ccol1%2Chigh%2Clow%2Ccol2%27&format=json",
	      dataType: "json",
	      async: true,
	      success : function(data){
	      var symbol;
	      var ibove = "^BVSP";
	      var nasdaq = "^IXIC";
	      var sp500 = "^GSPC";
	      var quotes = $("#quotes");
	      $.each(data.query.results.row, function(index, value) { 
	           if (value.symbol == ibove){
	        symbol = "ibove"
	           }
	     else if (value.symbol == nasdaq){
	        symbol = "nasdaq"
	           }
	     else if (value.symbol == sp500){
	        symbol = "sp500"
	           }
	           div = $("#" + symbol);	           
	           console.log
	           div.find(".price span").text(value.price);
	           div.find(".change span").text(value.change);
	           var percentage = (value.change / value.price) * 100;
	           if (value.change > 0)
		           div.addClass('positive');
	           else 
		           div.addClass('negative');
	           div.find(".change").find(".value").append ("(" + percentage.toFixed(2)+"%)");
	      });//end each            
	   }//end function(data)
	  });//end getJSON
	  $.getJSON("http://www.google.com/finance/info?infotype=infoquoteall&q=^DJI&callback=?", function(value){
	     div = $("#dowJones");
	     value = value[0];
	     if (value.c >0)
			 div.addClass('positive');
         else 
	         div.addClass('negative');
         div.find(".price span").text(value["l_cur"]);
         div.find(".change span").text(value.c + " ("+value.cp+")");     
	 });
	  setTimeout("getQuotes()",10000);
	};//end function
	getQuotes();
</script>
