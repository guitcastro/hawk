<?php echo $this->Html->script('/js/tiny_mce/tiny_mce.js', array('inline' => false));?>
<div class="events form">
	<?php echo $this->Form->create('Event');?>
	<fieldset>
		<legend>
			<?php echo __('Adicionar Evento'); ?>
		</legend>
		<?php
		echo $this->Form->input('title' , array ('label' => 'Título'));
		echo $this->Form->input('description',array ('label' => 'Descrição' , 'id' => 'tinyMCE'));
		echo $this->Form->select('type',
				array ('workshop' => 'workshop','curso' => 'curso','palestra' => 'palestra' ),
				array ('empty' => false)
		);
		?>
	</fieldset>
	<div id="datepicker"></div>
	<div id="dates"></div>
	<?php echo $this->Form->end(__('Salvar'));?>
</div>

<script type="text/javascript">
$('#datepicker').multiDatesPicker({
	dateFormat: 'dd/mm/yy',
	dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado','Domingo'],
	dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
	dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
	monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro', 'Outubro','Novembro','Dezembro'],
	monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set', 'Out','Nov','Dez'],
	nextText: 'Próximo',
	prevText: 'Anterior',
	onSelect: function(dateText, inst) {
		var dates = $('#datepicker').multiDatesPicker('getDates');
		var list = $('#dates').empty()
		$.each(dates, function (i, d) {
            var input = $('<input>');
            var inputHidden = $('<input>');
            var dateArray = d.split('/');
            $(input).attr('value',d);     
            $(input).attr('value',d);
            $(input).attr('name','Days['+i+']');   
            $(inputHidden).attr('value', dateArray[2] + '-' + dateArray[1] + '-' + dateArray[0] + ' 00:00:00');
            $(inputHidden).attr('type','hidden');
            $(inputHidden).attr('name','day['+i+']');  
			list.append(input);
            list.append(inputHidden);		
		});
	}});

</script>
<script type="text/javascript">
tinyMCE.init({
        // General options
        mode: 'exact',
        elements : 'tinyMCE',
        theme : "advanced",
        plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

        // Theme options
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        height: '500px'
});
</script>