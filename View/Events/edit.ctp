<?php echo $this->Html->script('/js/tiny_mce/tiny_mce.js', array('inline' => false));?>
<div class="events form">
<?php echo $this->Form->create('Event');?>
	<fieldset>
		<legend><?php echo __('Editar Evento'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('title' , array ('label' => 'Título'));
		echo $this->Form->input('description',array ('label' => 'Descrição' , 'id' => 'tinyMCE'));
		echo $this->Form->select('type',
				array ('workshop' => 'workshop','curso' => 'curso','palestra' => 'palestra' ),
				array ('empty' => false)
		);
	?>
	</fieldset>
<?php echo $this->Form->end(__('Enviar'));?>
</div>
<script type="text/javascript">
tinyMCE.init({
        // General options
        mode: 'exact',
        elements : 'tinyMCE',
        theme : "advanced",
        plugins : "autolink,lists,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

        // Theme options
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        height: '500px'
});
</script>
