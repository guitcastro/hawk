<div class="events index public">
	<?php
	if (isset ($allowedActions['EVENTS']))
		echo $this->Html->link(__('adicionar novo evento'), array('controller' => 'events', 'action' => 'add'), array('class' => 'admin top btn'));
	?>
	
	<h2></h2>
	<?php
	$i = 0;
	foreach ($events as $event): ?>
	<div class="event">
		<div class="event-header">
			<h3>
				<?php echo h($event['Event']['title']); ?>
				<span class="admin btn edit">
				<?php echo $this->Html->link ('editar', array ('controller' => 'events' , 'action' => 'edit', $event['Event']['id']))?>
				<?php echo $this->Form->postLink(__('excluir'),array('controller' => 'events' , 'action' => 'delete', $event['Event']['id']), null, __('Tem certeza que deseja apagar esse item?')); ?>
				</span>
			</h3>
			<div class="horario-box">
				<span class="horario"><?php echo h($event['Event']['type']); ?>
				</span>
			</div>
			<!--/horario-box-->
		</div>
		<!--/event-header-->
		<div class="description-box">
				<?php echo $event['Event']['description']; ?>
		</div>
		<!--/description-box-->
	</div>
	<!--/event-->
	<?php endforeach; ?>
</div><!--/events index-->
