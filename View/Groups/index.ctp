<div class="index">
	<!-- add new User Group -->	
    <?php echo $this->Form->create('Groups',array('action' => 'add')); ?>
    <h2><?php echo __('Adicionar Novo Grupo'); ?></h2>
    <?php
    	echo $this->Form->input('Aro.alias', array('label' => __('Nome do Grupo')));
    	echo $this->Form->end(__('Adicionar'));
    ?>
    <!-- List Users Groups -->
    <h2><?php echo __('Grupos de Usuários:'); ?></h2>
    <div class="accordion">
    <?php foreach ($groups as $group) { ?>
    	<h1>
    	<a href="#"> <?php echo __('Grupo') . ' - ' . $group['alias']; ?></a>
    	            <?php echo $this->Form->postLink(__('[x]'), array('action' => 'delete', $group['id']), null, __('Você tem certeza que deseja excluir o grupo %s?', $group['alias']));?>
    	</h1>
    	
    	<?php echo $this->Form->create('Groups',array('action' => 'edit'));?>
    		<table >
    			<thead>
    				<tr>
    					<th><?php echo __('Object') ?></th>
    					<th colspan="100%"><?php echo __('Actions') ?></th>
    				</tr>
    			</thead>
    			<tbody>    				    				    				    			
    			<?php foreach ($group['controllers'] as $controller) {?>
    				<tr>    	
    				<?php  if ($controller['actions']){?>
    					<td>
    					<?php
    					echo $this->Html->link($controller['alias'], array(
    	                                'controller' => $controller['alias'],
    	                                'action' => 'index'));
    					?>
    					</td>
    	                    <?php } ?>
    	                        <?php foreach ($controller['actions'] as $action) {?>
    	                        <td>                        
    								<?php
    								echo __($action['alias']);
    								echo '<br />';
    								echo $this->Form->checkbox('Group.' . $group['alias'] .'.' . $controller['alias'].'.'.$action['alias']
    	                            , array('checked' => $action['permission'])); ?>
    	                        </td>
    	                        <?php }?>
    	                    </tr>
    	                <?php } ?>   
    	         </tbody>
    		</table>
    	        <?php
    	        echo $this->Form->end('Salvar');
    	        ?>
    <?php  } ?>
	</div>        
    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('Anteriores'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('Próximos') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>
<script>
	$(function() {
		$( ".accordion" ).accordion({
			collapsible: true,
			active : false 	
		});
	});
</script>