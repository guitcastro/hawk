<?php

class UIHelper extends AppHelper {

	public $helpers = array('Html','Form','Js');

	/**
	 * If theres a data in request create inputs
	 * useful for edit methods
	 * @param $deleteUrl if you want do delete content via ajax, specif the action to delete
	 */
	private function createChildrenInputs ($fieldName,$deleteUrl = null,$options = array ()){		
		$options = array_merge (array ('div' => false, 'label' => false),$options);		
		//include an id field,or cake will generate new records instead of edit the current one
		$idFieldName= substr($fieldName, 0, strrpos ($fieldName,'X') +1) . '.id';
		//initialize variable for loop
		$i = 0;
		$newFieldName = str_replace('X',$i,$fieldName);
		$NewIdFieldName=  str_replace('X',$i,$idFieldName);
		$inputs = "";		
		while (Set::check($this->request->data,$newFieldName)){
			//html name attribute
			$htmlFieldName = current ($this->_name(null,$newFieldName));
			$NewIdHtmlFieldName = current ($this->_name(null,$NewIdFieldName));
			//the input
			$input = $this->Form->input ($newFieldName,$options);
			//the input id, to prevent cake create new records
			$inputID = $this->Form->input ($NewIdFieldName , array ('type' => 'hidden'));
			//create a button for delete record
			$uuid = $this->_View->uuid('button',array ($this->params['controller'],$this->params['action']));
			$button = $this->Form->button('-', array ('type' => 'button', 'id' => $uuid));
			//the item id
			$id = Set::classicExtract($this->request->data,$NewIdFieldName);

			//bind event
			$this->Js->get("#$uuid");
			//if enable ajaxDelete
			$ajax = '';
			if ($deleteUrl){
				array_push ($deleteUrl , $id);				
				$ajax = $this->Js->request($deleteUrl,array ('method' => 'post'));
			}	
			array_pop ($deleteUrl);
			$this->Js->event('click',
					$ajax .
					"jQuery('input[name=\"$htmlFieldName\"]').remove();
					jQuery('input[name=\"$NewIdHtmlFieldName\"]').remove();
					jQuery(this).remove();"
			);

			//next field
			$i++;
			$newFieldName = str_replace('X',$i,$fieldName);
			$NewIdFieldName = str_replace('X',$i,$idFieldName);
				
			//increment inputs
			$inputs .= $input . $button .$inputID ;
		}
		return $inputs;

	}

	/**
	 *
	 * @param string $fieldName the fieldName must be Email.X.email
	 * @param array  $options
	 * 				 'deleteUrl' => recive an cake url (string or array), if you want to enable ajax delete 
	 * 								for example array ('deleteUrl' => array ('action' => 'deleteEmail'));
	 */
	public function hasManyInput ($fieldName,$options=array()){

		$defaultOptions = array ('div' => false);
		$options = array_merge ($defaultOptions,$options);	

		//genarate a random uuid for javascript function name
		$uuid = $this->_View->uuid('input',array ($this->params['controller'],$this->params['action']));
		$input = $this->Form->input ($fieldName,$options);
		$button = $this->Form->button('+',array ('type'=>'button','onclick'=>"add$uuid()"));
		
		$deleteUrl = isset ($options['deleteUrl']) ? ($options['deleteUrl']) : array (); 
		//if request have data for this input, create it
		$childInputs = $this->createChildrenInputs($fieldName,$deleteUrl,$options);
		$div = $this->Html->div (null, $input.$button .$childInputs, array ('id' => $uuid));

		$fieldName = current ($this->_name(null,$fieldName));
		$script  =
		"function add$uuid(){
		var parentInput = jQuery('input[name=\"$fieldName\"]');
		if (!parentInput.val()){
		alert('Favor preencher o campo');
		return;
	}
	var div = $('#$uuid');
	var input = parentInput.clone();
	var button = $('<button>');
	var length = div.children().length;
	var inputName = parentInput.attr('name').replace('X',length);

	input.attr ('name',inputName);
	
	button.text('-');
	button.attr('type','button');
	button.bind('click',function () {
	jQuery('input[name=\"'+inputName+'\"]').remove();
	jQuery(this).remove();
	});
	parentInput.val('');
	div.append(input);
	div.append(button);
	};";
		$this->Js->buffer($script);
		return $div;
	}
}
?>