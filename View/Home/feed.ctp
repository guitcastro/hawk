<div class="inside feed">
	<ul>
		<?php
		foreach ($news as $new){
			$newsDate = isset($new['pubDate']) ? $new['pubDate'] :$new['a10:updated'];
			$time = date ('H:i' ,strtotime ($newsDate));
			?>
		<li>
			<div class="new">
				<span class="time"><?php echo $time  ?> </span>
				<?php echo $this->Html->link($new['title'],$new['link'],array ('class'=> 'call','target'=>'_blank'))?>
				<span class="content"><?php echo substr($new['description'],0,100)?>
					...</span> <span class="clear"> </span>
			</div>
		</li>
		<?php }?>
	</ul>
</div>
