<div id="carousel-container" class="container">
	<div class="border-box">
		<div class="inside">

			<?php echo $this->element('carousel', array ('images'=>$images));?>

		</div>
		<!--/inside-->
	</div>
	<!--/box-->
</div>
<!--/carousel-contairner-->

<div id="twitter-container" class="container">
	<span class="title">Twitter</span> <span class="twitter underline"></span>
	<div id="twitter" class="border-box">
		<div class="inside">
			<script>
      new TWTR.Widget({
        version: 2,
        type: 'profile',
        rpp: 3,
        interval: 30000,
        width: 235,
        height: 300,
        theme: {
          shell: {
            background: '#ffffff',
            color: '#003F5E'
          },
          tweets: {
            background: '#ffffff',
            color: '#999999',
            links: '#003F5E'
          }
        },
        features: {
          scrollbar: false,
          loop: true,
          live: false,
          behavior: 'all'
        }
      }).render().setUser('hawkinvest').start();
      </script>
		</div>
		<!--/inside-->
	</div>
	<!--/twitter-->
</div>
<!--/twitter-container-->

<div id="news-container" class="container">
	<span class="title">Últimas notícias</span> <span
		class="news underline"></span>
	<div id="news" class="border-box">
		<div id="tabs">
			<ul>
				<li><a href="/home/feed/estadao">Estadão</a></li>
				<li><a href="/home/feed/exame">Exame</a></li>
				<li><a href="/home/feed/infomoney">InfoMoney</a></li>
			</ul>
		</div><!--/tabs-->
	</div><!--/news-->
</div><!--/news-container-->
<script>
	$(function() {
		$( "#tabs" ).tabs({
			ajaxOptions: {
				error: function( xhr, status, index, anchor ) {
					$( anchor.hash ).html(
						"Não foi possível ler o feed" );
				},
				async : false
			},
			show: function(event, ui) {
				var feed = $(ui.panel).find('.feed');
				$(feed).jcarousel({
				    vertical: true
				});
			}
		});
	});
</script>