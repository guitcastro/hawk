<div class="images form">
<?php echo $this->Form->create('Image', array('type' => 'file'));?>
	<fieldset>
		<legend><?php echo __('Adicionar imagem'); ?></legend>
	<?php
		echo $this->Form->input('title', array ('label' => __('Título')));
		echo $this->Form->input('description',array ('label' => __('Descrição')));
		echo $this->Form->input('link');
		echo $this->Form->file('file');		
                echo $this->Form->input('from',array ('type'=>'hidden'));
                echo $this->Form->input('until',array ('type'=>'hidden'));
	?>
                <div>
                    <div id="from">Do dia: </div>
                    <div id="until">Ao dia: </div>
                </div>
	</fieldset>
<?php echo $this->Form->end(__('Enviar'));?>
</div>
<?php
    $script =  '$("#from").datepicker({
                dayNames: ["Domingo","Segunda","Terça","Quarta","Quinta","Sexta","Sábado","Domingo"],
                dayNamesMin: ["D","S","T","Q","Q","S","S","D"],
                dayNamesShort: ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb","Dom"],
                monthNames: ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro", "Outubro","Novembro","Dezembro"],
                monthNamesShort: ["Jan","Fev","Mar","Abr","Mai","Jun","Jul","Ago","Set", "Out","Nov","Dez"],
                nextText: "Próximo",
                prevText: "Anterior",
                altField: "#ImageFrom",
                altFormat: "yy-mm-dd"})
                $("#until").datepicker({
                dayNames: ["Domingo","Segunda","Terça","Quarta","Quinta","Sexta","Sábado","Domingo"],
                dayNamesMin: ["D","S","T","Q","Q","S","S","D"],
                dayNamesShort: ["Dom","Seg","Ter","Qua","Qui","Sex","Sáb","Dom"],
                monthNames: ["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro", "Outubro","Novembro","Dezembro"],
                monthNamesShort: ["Jan","Fev","Mar","Abr","Mai","Jun","Jul","Ago","Set", "Out","Nov","Dez"],
                nextText: "Próximo",
                prevText: "Anterior",
                altField: "#ImageUntil",
                altFormat: "yy-mm-dd"})
                ';
    $this->Js->buffer($script);
?>