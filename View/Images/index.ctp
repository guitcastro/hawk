<div class="images index">
	<h2>
	<?php echo __('Imagens');?></h2>
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('path');?>
			</th>
			<th><?php echo $this->Paginator->sort('Título');?>
			</th>
			<th><?php echo $this->Paginator->sort('Descrição');?>
			</th>
			<th><?php echo $this->Paginator->sort('Data de criação');?>
			</th>
			</th>
			<th class="actions"><?php echo __('Ações');?>
			</th>
		</tr>
		
		
	<?php
	$i = 0;
	foreach ($images as $image): ?>
	<tr>
		<td><?php echo h($image['Image']['webPath']); ?>&nbsp;</td>
		<td><?php echo h($image['Image']['title']); ?>&nbsp;</td>
		<td><?php echo h($image['Image']['description']); ?>&nbsp;</td>
		<td><?php echo h($image['Image']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Form->postLink(__('Excluir'), array('action' => 'delete', $image['Image']['id']), null, __('Tem certeza que deseja apagar a imagem %s?', $image['Image']['title'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>

	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Página {:page} de {:pages}, mostrando {:current} registro de um total de {:count}')
	));
	?>
	</p>

	<div class="paging">

	<?php
	echo $this->Paginator->prev('< ' . __('Anteriores'), array(), null, array('class' => 'prev disabled'));
	echo $this->Paginator->numbers(array('separator' => ''));
	echo $this->Paginator->next(__('Próximas') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<ul>
		<li><?php echo $this->Html->link(__('Nova Imagem'), array('action' => 'add')); ?>
		</li>
	</ul>
</div>
