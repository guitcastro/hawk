<!DOCTYPE html >
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="UTF-8" />
<title><?php ?> <?php echo $title_for_layout; ?>
</title>
<?php
echo $this->Html->meta('fav-icon.png');
echo $this->Html->css(array ('cake.generic','reset', 'arctic', 'style','carousel'));
echo $this->Html->script(array ('asset','twitter','jquery.placeholder.min'));
//echo $this->Html->script(array ('jquery-1.6.2.min','jquery.jeditable','jquery-ui.multidatespicker','jquery-ui-1.8.17.custom.min', 'jquery.jcarousel.min'));
echo $scripts_for_layout;
?>
<link rel="shortcut icon"
	href="<?php echo $this->webroot; ?>img/fav-icon.png" type="image/png" />
</head>
<body>
	<div class="wrapper">
		<div id="header">
			<span id="hawk-logo"></span> <span id="citi-logo"></span> <span
				class="clear"></span>
		</div>
		<!--/header-->
		<div id="nav-bar">
			<div class="left nav arrow"></div>
			<!--/nav-arrow-left-->
			<div class="right nav arrow"></div>
			<!--/nav-arrow-right-->
			<div id="menu">
				<ul>
					<li class=""><?php echo $this->Html->link('Home',array ('controller' => 'home'), array( 'class' => 'item'))?>
					</li>
					<li class=""><?php echo $this->Html->link('Quem Somos', array ('controller' => 'pages' , 'action' => 'display', 'quemsomos'),array ('class' => 'item')); ?>
					</li>				
					<li class="relative"> <a href="#" class="item">Investimentos</a>
						<div class="drop-down">
							<ul>
								<li><?php echo $this->Html->link('Renda variável', array ('controller' => 'pages' , 'action' => 'display', 'rendavariavel')); ?>
								</li>
								<li><?php echo $this->Html->link('Renda fixa', array ('controller' => 'pages' , 'action' => 'display', 'rendafixa')); ?>
								</li>
								<li><?php echo $this->Html->link('Mercados futuros', array ('controller' => 'pages' , 'action' => 'display', 'mercadosfuturos')); ?>
								</li>
								<li><?php echo $this->Html->link('Clube de investimentos', array ('controller' => 'pages' , 'action' => 'display', 'clubedeinvestimentos')); ?>
								</li>
								<li><?php echo $this->Html->link('Como começar', array ('controller' => 'pages' , 'action' => 'display', 'comecomecar')); ?>
								</li>
							</ul>
						</div> <!--/drop-down-->
					</li>
					<li class="relative"><a href="#" class="item">Educacional</a>
						<div class="drop-down">
							<ul>
								<li><?php echo $this->Html->link('Cursos', array ('controller' => 'events' , 'action' => 'filter', 'curso'));?></li>
								<li><?php echo $this->Html->link('Workshops', array ('controller' => 'events' , 'action' => 'filter', 'workshop'));?></li>
								<li><?php echo $this->Html->link('Palestra', array ('controller' => 'events' , 'action' => 'filter', 'palestra'));?></li>
							</ul>
						</div> <!--/drop-down-->
					</li>
				</ul>

			</div>
			<!--/menu-->
			<?php 
			if (!$this->Session->read('Auth.User'))
				echo $this->element('login');
			else
				echo $this->element('actions');
			?>
			<span class="clear"></span>
		</div>
		<!--/nav-bar-->
		<div id="main">
			<div id="content">
				<?php echo $this->Session->flash(); ?>
				<?php echo $content_for_layout; ?>
				<span class="clear"></span>
			</div>
			<!--/content-->
			<div id="right-column">
				<div id="calendar-container" class="container">
					<span class="title">Calendário de cursos</span> <span
						class="calendar underline"></span>
					<div id="calendar" class="border-box">
						<div class="inside">
							<?php echo $this->element('envetsCalendar') ?>
							<div id="legenda">
								<ul>
									<li><?php echo $this->Html->image('hoje-ico.png',array ('class'=> 'ico','alt'=>'palestra')); ?><span>Hoje</span>
									</li>
									<li><?php echo $this->Html->image('workshop-ico.png',array ('class'=> 'ico','alt'=>'workshop')); ?><span>Workshop</span>
									</li>
								</ul>
								<ul>
									<li><?php echo $this->Html->image('palestra-ico.png',array ('class'=> 'ico','alt'=>'palestra')); ?><span>Palestra</span>
									</li>
									<li><?php echo $this->Html->image('curso-ico.png',array ('class'=> 'ico','alt'=>'curso')); ?>
										<span>Curso</span>
									</li>
								</ul>
								<span class="clear"></span>
							</div>
							<!--/legenda-->
						</div>
						<!--/inside-->
					</div>
					<!--/calendar-->
				</div>
				<!--/calendar-container-->
				<span class="clear"></span>
				<div id="indices-container" class="container">
					<span class="title">Cotações da Bolsa</span> <span
						class="indices underline"></span>
					<div id="indices" class="border-box">
						<div class="inside">
							<?php echo $this->element('quotes') ?>
						</div>
						<!--/inside-->
					</div>
					<!--/indices-->
				</div>
				<!--/indices-container-->
			</div>
			<!--/right-column-->
			<span class="clear"></span>
		</div>
		<!--/main-->
		<div id="footer">
			<div id="top-border"></div>
			<div id="title">
				Entre em contato <span class="arrow"></span>
			</div>
			<!--/title-->
			<div class="first container">
				<ul>
					<li><a class="fb footer-btn">Hawk no facebook</a></li>
					<li><a href="" class="tw footer-btn">Hawk no twitter</a></li>
					<li><a id="bookmark" href="http://hawkinvestimentos.com.br"
						title="Hawk Investimentos" class="fav footer-btn">Adicionar aos
							favoritos</a></li>
				</ul>
				<a href="#" id="back-to-top">Voltar ao topo</a>
			</div>
			<!--/first-container-->
			<div class="mail-form container">

				<form method="POST" action="mailto:someone@$nailmail.com"
					enctype="text/plain">
					<fieldset>

						<label for="data[mail-form][de]"> <?php echo $this->Html->image('footer-mail-ico.png');?>
						</label>
						<?php echo $this->Form->input('de', array(
								'type' => 'email',
								'div' => false,
								'placeholder' => 'Seu e-mail',
								'class' => 'username field',
								'label' => false)); ?>

						<?php 
						echo $this->Form->input('seu e-mail', array(
								'type' => 'text',
								'placeholder' => 'Assunto',
								'class' => 'field',
								'label' => false));

						echo $this->Form->input('assunto', array(
								'type' => 'textarea',
								'placeholder' => 'Mensagem',
								'class' => 'field',
								'label' => false));?>
						<div class="rtl">
							<?php 
							echo $this->Form->submit('Limpar', array( 'type' => 'reset',
									'class' => 'submit field clean',
									'label' => false,
									'div' =>false
							));
							echo $this->Form->submit('Enviar', array( 'type' => 'submit',
									'class' => 'submit field',
									'div' =>false,
									'label' => false
							));?>
						</div>
					</fieldset>
				</form>

			</div>
			<!--/mail-form container-->
			<div class="contact container">
				<div class="item">
					<?php echo $this->Html->image('footer-location-ico.png',array ('class' => 'ico','alt'=>'location-ico'));?>
					<span class="">Rua Pernambuco, 353 - Salas 806/807<br />Savassi,
						Belo Horizonte, MG
					</span> <span class="clear"></span>
				</div>
				<!--/item-->
				<div class="item middle">
					<?php echo $this->Html->image('footer-phone-ico.png',array ('class' => 'ico','alt'=>'"phone-ico'));?>
					<span class="contact-txt">(31) 3289-8500</span> <span class="clear"></span>
				</div>
				<!--/item-->
				<div class="item">
					<?php echo $this->Html->image('footer-mail-ico.png',array ('class' => 'ico','alt'=>'"mail-ico'));?>
					<span class="contact-txt"><a
						href="mailto:contato@hawkinvestimentos.com.br">contato@hawkinvestimentos.com.br</a>
					</span> <span class="clear"></span>
				</div>
				<!--/item-->
			</div>
			<!--/contact container-->
			<span class="clear"></span>
			<div id="signature">
				<span>Página criada por Guilherme Torres e Nathália Xavier</span>
			</div>
			<!--/signature-->
		</div>
		<!--/footer-->
	</div>
	<!--/wrapper-->
	<script type="text/javascript">

	/* ANIMAÇÕES NAV-BAR */

	$('#login-btn').click(function(){
		$(this).parents('#login').find('.dashboard').slideToggle('fast');
		$(this).find('#arrow').toggleClass('hover');
  });

  $('#login-btn').hover(function(){
		$(this).toggleClass('hover')
  });

  $('#menu .relative').mouseenter(function(){
  	if ($('.drop-down:visible')) {
  		$('#menu .drop-down').slideUp('fast');
  	}
		$(this).find('.drop-down').slideDown('fast');
		return false;
  });

  $('#nav-bar, #menu .relative').mouseleave(function(){
  	if ($('.drop-down:visible')) {
  		$('#menu .drop-down').slideUp('fast');
  	}
  });

  $('#menu .drop-down').mouseleave(function(){
  	$(this).slideUp('fast');
  });

  /* BOTÃO "VOLTAR AO TOPO" */

	$('#back-to-top').click(function(){
	  $('html, body').animate({scrollTop: '0px'}, 1000);
	  return false;
	});

	/* PLACEHOLDER CROSSBROWSER - CHAMA PLUGIN */
	
	$('input[placeholder], textarea[placeholder]').placeholder();
	
	$("#bookmark").click(function(event){
		event.preventDefault(); // prevent the anchor tag from sending the user off to the link
		var url = this.href;
		var title = this.title;
		if (window.sidebar) { // Mozilla Firefox Bookmark
			window.sidebar.addPanel(title, url,"");
		} else if( window.external ) { // IE Favorite
			window.external.AddFavorite( url, title);
		} else if(window.opera) { // Opera 7+
			return false; // do nothing - the rel="sidebar" should do the trick
		} else { // for Safari, Konq etc - browsers who do not support bookmarking scripts (that i could find anyway)
			 alert('Infelizmente seu navegador não suporta essa ação, por favore adicione nosso siste ao favorito '
			 + ' please bookmark this page manually.');
		}

	});
	</script>

	<?php echo $this->Js->writeBuffer(array('onDomReady' => false));?>
	<div id="placeHolder"></div>
</body>
</html>
