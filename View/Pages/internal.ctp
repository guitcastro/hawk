<?php 
if (isset ($allowedActions['PAGES'])) 
echo $this->Html->link ('Editar página',array ('controller' => 'pages' , 'action' => 'edit' ,$page['InternalPage']['id'] ), array('class' => 'admin btn'));
?>
<div class="index public">
	<h2>
		<?php echo __($page['InternalPage']['name']);?>
	</h2>
	<span class="underline"></span>
	<div class="description">
		<?php echo $page['InternalPage']['content'];?>
	</div>
	<!--/description-->
</div>
