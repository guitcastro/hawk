
<div class="reports form">
<?php echo $this->Form->create('Report',array('type' => 'file'));?>
	<fieldset>
		<legend><?php echo __('Add Report'); ?></legend>
	<?php
		echo $this->Form->input('title', array ('label' => 'Título'));		
		echo $this->Form->input('description',array ('label' => 'Descrição'));
		echo $this->Form->file('file', array ('label' => 'Arquivo'));
		echo $this->Form->input('tags', array ('label' => 'Tags'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit'));?>
</div>
