<div class="relatorios index public">

	<?php
	if (isset ($allowedActions['REPORTS']['ADD']))
		echo $this->Html->link(__('Novo Relatório'), array('action' => 'add'), array('class' => 'admin top btn') ); ?>
	<h2>
		<?php echo __('Relatórios');?>
	</h2>
	<span class="relatorios underline"></span>

	<div id="relatorios-box">
		<?php /*
		foreach ($reports as $report): ?>
	<!--/VERSAO DE RELATORIO COM DESCRICAO-->
	<div class="relatorio">
	<span class="relatorio-icon"></span>
	<div class="relatorio-header">
	<h3><?php echo $this->Html->link($report['Report']['title'], $report['Report']['webPath']); ?></h3>
	<div class="tags-box">
	<span class="tag">Tags:</span>
	<span class="tag"><a href="">ações</a></span>
	<span class="tag"><a href="">mercado</a></span>
	<span class="tag"><a href="">dinehiro</a></span>
	<span class="tag"><a href="">capetalismo</a></span>
	<span class="tag"><a href="">ganância</a></span>
	<span class="tag"><a href="">criasalimento</a></span>
	<span class="tag"><a href="">ganância</a></span>
	<span class="tag"><a href="">criasalimento</a></span>
	<span class="tag"><a href="">ganância</a></span>
	<span class="tag"><a href="">criasalimento</a></span>
	</div><!--/tags box-->
	</div><!--/relatorio-header-->
	<div class="description-box">
	<p><?php echo h($report['Report']['description']); ?></p>
	<span class="clear"></span>
	</div><!--/description-box-->
	<span class="clear"></span>
	</div><!--/relatorio-->
	<?php endforeach; */?>
		<?php
		foreach ($reports as $report): ?>
		<!--/VERSAO DE RELATORIO COM BOTAO DE DOWNLOAD-->
		<div class="relatorio">
			<span class="relatorio-icon"></span>
			<div class="relatorio-header">
				<h3>
					<a href="DOWNLOAD"><?php echo h($report['Report']['title']); ?>
					</a> <span class="admin btn delete"><?php echo $this->Form->postLink(__('Excluir'), array('action' => 'delete', $report['Report']['id']), null, __('Você tem certeza que deseja excluir o relatório:  %s?', $report['Report']['title'])); ?>
					</span>
				</h3>
				<div class="tags-box">
					<span class="tag">Tags:</span>
					<?php foreach ($report['Tag'] as $tag) { ?>
					<span class="tag"> <?php echo $this->Html->link($tag['name'],'#');?>
					</span>
					<?php }?>
				</div>
				<!--/tags box-->
			</div>
			<!--/relatorio-header-->
			<div class="download-box">
				<?php echo $this->Html->link('Download', $report['Report']['webPath'], array('class' => 'download-btn')); ?>
			</div>
			<!--/download-box-->
			<span class="clear"></span>
		</div>
		<!--/relatorio-->
		<?php endforeach; ?>
		<div class="paging">
			<?php
			echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => ''));
			echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
			?>
		</div>
		<!--/paging-->
	</div>
	<!--/relatorios-box-->
</div>
<!--/relatiorios index-->
