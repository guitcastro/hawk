<div class="users form">
	<?php echo $this->Form->create('User');?>
	<fieldset>
		<legend>
			<?php echo __('Add User'); ?>
		</legend>
		<?php	
		$radio = $this->Form->radio ("active",array (1 => 'Ativo' ,0 =>'Inativo'),array ('legend'=>false,'value' => 1)); 		
		echo $this->Form->input('username',array ('label'=>'login','div' => 'required'));
		echo $this->Form->input('name',array ('label'=>'Nome','div' => 'required'));
		echo $this->Form->select('Aro.id',$aros ,array ('label'=>'Grupo','empty' => false));
		echo $this->Html->div ("radio",	$radio );
		echo $this->UI->hasManyInput('Email.X.email');
		echo $this->UI->hasManyInput('Phone.X.number');
		?>
	</fieldset>
	<?php echo $this->Form->end(__('Submit'));?>
</div>

