<div class="users index">
	<table>
		<tr>
			<th><?php echo $this->Paginator->sort('username','login');?>
			</th>
			<th><?php echo $this->Paginator->sort('name','nome');?>
			</th>
			<th><?php echo $this->Paginator->sort('active','ativo');?>
			<th><?php echo $this->Paginator->sort('created','criado');?>
			</th>
			
			<th><?php echo $this->Paginator->sort('aro.alias','Group');?></th>
			<th class="actions"><?php echo __('Actions');?></th>
		</tr>
		<?php
		$i = 0;
		foreach ($users as $user): ?>
		<tr>
			<td contenteditable="true"><?php echo h($user['User']['username']); ?>&nbsp;</td>
			<td contenteditable="true"><?php echo h($user['User']['name']); ?>&nbsp;</td>
			<td><?php echo h($user['User']['active']); ?>&nbsp;</td>
			<td><?php echo h($this->Time->format('d/m/Y', $user['User']['created'])); ?>&nbsp;</td>
			<td><?php  echo $this->Html->link($user['Aro']['alias'], array('controller' => 'aros', 'action' => 'view', $user['Aro']['id'])); ?>
			</td>
			<td class="actions">
					<?php echo $this->Js->link('Emails',array (
							'controller'=> 'users',
							'action'=> 'showEmails',$user['User']['id']),
							array ('update' => '#placeHolder')
					);
					?>
					<?php echo $this->Js->link('Telefones',array (
							'controller'=> 'users',
							'action'=> 'showPhones',$user['User']['id']),
							array ('update' => '#placeHolder')
					);
					?>
					<?php echo $this->Form->postLink(__('Excluir'), array(
							'action' => 'delete', $user['User']['id']),
							null, __('Are you sure you want to delete # %s?',
									$user['User']['id'])); ?>
					<?php echo $this->Html->link(__('Editar'), array(
							'action' => 'edit', $user['User']['id']));?>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
	<p>
		<?php
		echo $this->Paginator->counter(array(
				'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
		));
		?>
	</p>

	<div class="paging">
		<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
		?>
	</div>

