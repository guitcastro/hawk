-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 26/02/2012 às 20h02min
-- Versão do Servidor: 5.5.19
-- Versão do PHP: 5.3.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `hawk`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `acos`
--

DROP TABLE IF EXISTS `acos`;
CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=37 ;

--
-- Extraindo dados da tabela `acos`
--

INSERT INTO `acos` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, NULL, NULL, 'controllers', 1, 72),
(2, 1, NULL, NULL, 'Events', 2, 13),
(3, 2, NULL, NULL, 'index', 3, 4),
(4, 2, NULL, NULL, 'view', 5, 6),
(5, 2, NULL, NULL, 'add', 7, 8),
(6, 2, NULL, NULL, 'edit', 9, 10),
(7, 2, NULL, NULL, 'delete', 11, 12),
(8, 1, NULL, NULL, 'Groups', 14, 23),
(9, 8, NULL, NULL, 'index', 15, 16),
(10, 8, NULL, NULL, 'add', 17, 18),
(11, 8, NULL, NULL, 'delete', 19, 20),
(12, 8, NULL, NULL, 'edit', 21, 22),
(13, 1, NULL, NULL, 'Home', 24, 33),
(14, 13, NULL, NULL, 'index', 25, 26),
(15, 13, NULL, NULL, 'findEvents', 27, 28),
(16, 13, NULL, NULL, 'feed', 29, 30),
(17, 13, NULL, NULL, 'email', 31, 32),
(18, 1, NULL, NULL, 'Images', 34, 45),
(19, 18, NULL, NULL, 'index', 35, 36),
(20, 18, NULL, NULL, 'view', 37, 38),
(21, 18, NULL, NULL, 'add', 39, 40),
(22, 18, NULL, NULL, 'edit', 41, 42),
(23, 18, NULL, NULL, 'delete', 43, 44),
(24, 1, NULL, NULL, 'Users', 46, 69),
(25, 24, NULL, NULL, 'login', 47, 48),
(26, 24, NULL, NULL, 'logout', 49, 50),
(27, 24, NULL, NULL, 'index', 51, 52),
(28, 24, NULL, NULL, 'view', 53, 54),
(29, 24, NULL, NULL, 'add', 55, 56),
(30, 24, NULL, NULL, 'edit', 57, 58),
(31, 24, NULL, NULL, 'delete', 59, 60),
(32, 24, NULL, NULL, 'showEmails', 61, 62),
(33, 24, NULL, NULL, 'showPhones', 63, 64),
(34, 24, NULL, NULL, 'deleteEmail', 65, 66),
(35, 24, NULL, NULL, 'deletePhone', 67, 68),
(36, 1, NULL, NULL, 'AclExtras', 70, 71);

-- --------------------------------------------------------

--
-- Estrutura da tabela `aros`
--

DROP TABLE IF EXISTS `aros`;
CREATE TABLE IF NOT EXISTS `aros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) DEFAULT NULL,
  `model` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `foreign_key` int(10) DEFAULT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lft` int(10) DEFAULT NULL,
  `rght` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(3, NULL, 'Group', NULL, 'admin', 1, 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `aros_acos`
--

DROP TABLE IF EXISTS `aros_acos`;
CREATE TABLE IF NOT EXISTS `aros_acos` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `aro_id` int(10) NOT NULL,
  `aco_id` int(10) NOT NULL,
  `_create` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `_read` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `_update` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `_delete` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ARO_ACO_KEY` (`aro_id`,`aco_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=37 ;

--
-- Extraindo dados da tabela `aros_acos`
--

INSERT INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`) VALUES
(1, 3, 1, '-1', '-1', '-1', '-1'),
(2, 3, 2, '-1', '-1', '-1', '-1'),
(3, 3, 3, '1', '1', '1', '1'),
(4, 3, 4, '1', '1', '1', '1'),
(5, 3, 5, '1', '1', '1', '1'),
(6, 3, 6, '1', '1', '1', '1'),
(7, 3, 7, '1', '1', '1', '1'),
(8, 3, 8, '-1', '-1', '-1', '-1'),
(9, 3, 9, '1', '1', '1', '1'),
(10, 3, 10, '1', '1', '1', '1'),
(11, 3, 11, '1', '1', '1', '1'),
(12, 3, 12, '1', '1', '1', '1'),
(13, 3, 13, '-1', '-1', '-1', '-1'),
(14, 3, 14, '1', '1', '1', '1'),
(15, 3, 15, '1', '1', '1', '1'),
(16, 3, 16, '1', '1', '1', '1'),
(17, 3, 17, '1', '1', '1', '1'),
(18, 3, 18, '-1', '-1', '-1', '-1'),
(19, 3, 19, '1', '1', '1', '1'),
(20, 3, 20, '1', '1', '1', '1'),
(21, 3, 21, '1', '1', '1', '1'),
(22, 3, 22, '1', '1', '1', '1'),
(23, 3, 23, '1', '1', '1', '1'),
(24, 3, 24, '-1', '-1', '-1', '-1'),
(25, 3, 25, '1', '1', '1', '1'),
(26, 3, 26, '1', '1', '1', '1'),
(27, 3, 27, '1', '1', '1', '1'),
(28, 3, 28, '1', '1', '1', '1'),
(29, 3, 29, '1', '1', '1', '1'),
(30, 3, 30, '1', '1', '1', '1'),
(31, 3, 31, '1', '1', '1', '1'),
(32, 3, 32, '1', '1', '1', '1'),
(33, 3, 33, '1', '1', '1', '1'),
(34, 3, 34, '1', '1', '1', '1'),
(35, 3, 35, '-1', '-1', '-1', '-1'),
(36, 3, 36, '-1', '-1', '-1', '-1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `days_events`
--

DROP TABLE IF EXISTS `days_events`;
CREATE TABLE IF NOT EXISTS `days_events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `day` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=58 ;

--
-- Extraindo dados da tabela `days_events`
--

INSERT INTO `days_events` (`id`, `event_id`, `day`) VALUES
(24, 27, '2012-01-18 00:00:00'),
(25, 28, '2012-02-02 00:00:00'),
(26, 29, '2012-01-18 00:00:00'),
(27, 30, '2012-02-10 00:00:00'),
(28, 30, '2012-02-11 00:00:00'),
(29, 30, '2012-02-12 00:00:00'),
(30, 30, '2012-02-13 00:00:00'),
(31, 30, '2012-02-14 00:00:00'),
(32, 30, '2012-02-15 00:00:00'),
(33, 30, '2012-02-16 00:00:00'),
(34, 30, '2012-02-17 00:00:00'),
(35, 30, '2012-02-18 00:00:00'),
(36, 31, '2012-02-01 00:00:00'),
(37, 31, '2012-02-08 00:00:00'),
(38, 31, '2012-02-09 00:00:00'),
(39, 31, '2012-02-12 00:00:00'),
(40, 31, '2012-02-13 00:00:00'),
(41, 31, '2012-02-15 00:00:00'),
(42, 31, '2012-02-16 00:00:00'),
(43, 31, '2012-02-18 00:00:00'),
(44, 31, '2012-02-19 00:00:00'),
(45, 31, '2012-02-21 00:00:00'),
(46, 31, '2012-02-25 00:00:00'),
(47, 31, '2012-02-26 00:00:00'),
(48, 31, '2012-02-27 00:00:00'),
(49, 31, '2012-02-28 00:00:00'),
(50, 31, '2012-02-29 00:00:00'),
(51, 32, '2012-02-09 00:00:00'),
(52, 32, '2012-02-14 00:00:00'),
(53, 32, '2012-02-15 00:00:00'),
(54, 32, '2012-02-17 00:00:00'),
(55, 32, '2012-02-20 00:00:00'),
(56, 32, '2012-02-25 00:00:00'),
(57, 32, '2012-02-26 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `emails`
--

DROP TABLE IF EXISTS `emails`;
CREATE TABLE IF NOT EXISTS `emails` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- Extraindo dados da tabela `emails`
--

INSERT INTO `emails` (`id`, `email`, `user_id`) VALUES
(17, 'dsadasdsadas@oi.com', 23),
(19, 'dsadasdsada@ds.com', 23);

-- --------------------------------------------------------

--
-- Estrutura da tabela `events`
--

DROP TABLE IF EXISTS `events`;
CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `created_id` int(11) NOT NULL,
  `modfied` datetime NOT NULL,
  `modified_id` int(11) NOT NULL,
  `horario` time NOT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT 'workshop , curso, palestra',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=33 ;

--
-- Extraindo dados da tabela `events`
--

INSERT INTO `events` (`id`, `title`, `description`, `created`, `created_id`, `modfied`, `modified_id`, `horario`, `type`) VALUES
(27, 'evento no mes de janeiro', '', '2012-01-26 21:39:42', 0, '0000-00-00 00:00:00', 0, '00:00:00', 'workshop'),
(28, 'Evento em fevereiro', '2', '2012-01-26 21:39:54', 0, '0000-00-00 00:00:00', 0, '00:00:00', 'workshop'),
(29, 'segundo evento dia 18 de janeiro', '', '2012-01-26 22:27:26', 0, '0000-00-00 00:00:00', 0, '00:00:00', 'workshop'),
(30, 'Exemplo de curso ', 'blá blá blá', '2012-02-03 15:14:25', 0, '0000-00-00 00:00:00', 0, '00:00:00', 'workshop'),
(31, 'curso', '', '2012-02-03 17:53:00', 0, '0000-00-00 00:00:00', 0, '00:00:00', 'curso'),
(32, 'palestra', '', '2012-02-03 17:53:21', 0, '0000-00-00 00:00:00', 0, '00:00:00', 'palestra');

-- --------------------------------------------------------

--
-- Estrutura da tabela `images`
--

DROP TABLE IF EXISTS `images`;
CREATE TABLE IF NOT EXISTS `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `webPath` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `created_id` int(10) unsigned NOT NULL,
  `modified_id` int(10) unsigned NOT NULL,
  `serverPath` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `from` date NOT NULL,
  `until` date NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Extraindo dados da tabela `images`
--

INSERT INTO `images` (`id`, `webPath`, `title`, `description`, `created`, `modified`, `created_id`, `modified_id`, `serverPath`, `from`, `until`, `link`) VALUES
(15, '/img/dadsa.jpg', 'images 1 preata', '', '2012-02-01 15:04:26', '2012-02-01 15:04:26', 0, 0, '/var/www/html/hawk/webroot/img/dadsa.jpg', '2012-02-01', '2012-09-04', ''),
(16, '/img/carousel-img.jpg', 'images 2', '', '2012-02-01 15:04:58', '2012-02-02 15:04:58', 0, 0, '/var/www/html/hawk/webroot/img/carousel-img.jpg', '2012-02-01', '2012-11-02', ''),
(17, '/img/dadsa.jpg', 'imagem com link', '', '2012-02-03 15:48:43', '2012-02-03 15:48:43', 0, 0, '/var/www/html/hawk/webroot/img/dadsa.jpg', '2012-02-01', '2012-05-31', 'www.google.com.br');

-- --------------------------------------------------------

--
-- Estrutura da tabela `logins`
--

DROP TABLE IF EXISTS `logins`;
CREATE TABLE IF NOT EXISTS `logins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `login_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `phones`
--

DROP TABLE IF EXISTS `phones`;
CREATE TABLE IF NOT EXISTS `phones` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Extraindo dados da tabela `phones`
--

INSERT INTO `phones` (`id`, `number`, `user_id`) VALUES
(7, 'dsa', 21),
(8, 'kdsjhajkdhsajk', 23);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `aro_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=24 ;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `active`, `created`, `updated`, `name`, `aro_id`) VALUES
(23, '380767', '763d329d11c505a324475db1db8dce5156835644', 1, '2012-02-22 21:04:11', '2012-02-26 19:43:46', 'Guilherme Torres Castro', 3);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
